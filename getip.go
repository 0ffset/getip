package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"reflect"
)

type IP struct {
	Status      string
	Country     string
	CountryCode string
	Region      string
	RegionName  string
	City        string
	Zip         string
	Lat         float64
	Lon         float64
	Timezone    string
	Isp         string
	Org         string
	As          string
	Query       string
}

func main() {
	ip, err := getip4()
	if err != nil {
		panic(err)
	}

	pPrintStruct(ip)
}

func pPrintStruct(s interface{}) {
	v := reflect.ValueOf(s)
	for i := 0; i < v.NumField(); i++ {
		fmt.Println(v.Type().Field(i).Name+":", v.Field(i))
	}
}

func getip4() (IP, error) {
	var ip IP

	req, err := http.Get("http://ip-api.com/json/")
	if err != nil {
		return ip, err
	}
	defer req.Body.Close()

	body, err := ioutil.ReadAll(req.Body)
	if err != nil {
		return ip, err
	}

	if err := json.Unmarshal(body, &ip); err != nil {
		return ip, err
	}
	return ip, nil
}
